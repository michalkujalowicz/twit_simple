Simple twitter clone with rest API build around observer pattern .
Run miku.twitter.Application to start Spring Boot container.

Operations:

Time line
GET http://localhost:8080/twitter/user/XXX/timeline
Wall 
GET http://localhost:8080/twitter/user/XXX/wall
Post twit
POST http://localhost:8080/twitter/user/XXX/twit - message in payload as a plain text
Follow other user
PUT http://localhost:8080/twitter/user/XXX - name of user to be followed in payload as a plain text

replace XXX with user name