package miku.twitter;

import miku.twitter.api.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AccountRepositoryTest {


    private static final String USERNAME_1 = "TEST_USER";
    private static final String USERNAME_2 = "TEST_USER2";


    @Test
    public void shouldCreateNewUser() {
        AccountRepository accountRepository = new AccountRepository();
        User user = accountRepository.findOrCreateAccount(USERNAME_1);

        assertEquals(USERNAME_1, user.getName());
    }

    @Test
    public void shouldReturnSameUserInSubsequentCalls() {
        AccountRepository accountRepository = new AccountRepository();
        User user = accountRepository.findOrCreateAccount(USERNAME_1);
        User sameUser = accountRepository.findOrCreateAccount(USERNAME_1);

        //In real world scenario user instances will be differentiable by UUID or similar autogenerated identifier
        assertTrue(user == sameUser);
    }

    @Test
    public void shouldHoldMultipleUsers() {
        AccountRepository accountRepository = new AccountRepository();
        User user1 = accountRepository.findOrCreateAccount(USERNAME_1);
        User user2 = accountRepository.findOrCreateAccount(USERNAME_2);

        assertEquals(user1, accountRepository.findOrCreateAccount(USERNAME_1));
        assertEquals(user2, accountRepository.findOrCreateAccount(USERNAME_2));


    }


}