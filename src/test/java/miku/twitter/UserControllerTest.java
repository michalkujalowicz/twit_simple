package miku.twitter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void shouldExposeWall() throws Exception {

        ResponseEntity<List> wallResponse = this.testRestTemplate.getForEntity(
                "http://localhost:" + this.port + "/twitter/user/Evans/wall", List.class);

        assertEquals(HttpStatus.OK, wallResponse.getStatusCode());
    }


    @Test
    public void shouldExposeTimeLine() throws Exception {

        ResponseEntity<List> wallResponse = this.testRestTemplate.getForEntity(
                "http://localhost:" + this.port + "/twitter/user/Evans/timeline", List.class);

        assertEquals(HttpStatus.OK, wallResponse.getStatusCode());
    }

    @Test
    public void shouldExposeFollowMethod() throws Exception {

        this.testRestTemplate.put(
                "http://localhost:" + this.port + "/twitter/user/Evans", "Marco");

    }

    @Test
    public void shouldPostMessageOnWall() throws Exception {

        ResponseEntity<List> postResponse = this.testRestTemplate.postForEntity(
                "http://localhost:" + this.port + "/twitter/user/Evans/twit", "sampleTwit", List.class);

        ResponseEntity<List> wallResponse = this.testRestTemplate.getForEntity(
                "http://localhost:" + this.port + "/twitter/user/Evans/wall", List.class);

        assertEquals(HttpStatus.OK, postResponse.getStatusCode());
        assertEquals(HttpStatus.OK, wallResponse.getStatusCode());
        assertEquals(1, wallResponse.getBody().size());
    }


}