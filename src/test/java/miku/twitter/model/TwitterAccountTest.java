package miku.twitter.model;

import miku.twitter.api.Twit;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TwitterAccountTest {

    private static final String SAMPLE_TWIT = "sample Twit";
    private static final String FOLLOWED_USER = "Evans";
    private static final String USER_NAME ="Vernon";


    @Test
    public void shouldReturnEmptyWall() {
        TwitterAccount user = new TwitterAccount(USER_NAME);
        assertTrue(user.getWall().isEmpty());
    }

    @Test
    public void ShouldReturnPostedTwit() {
        TwitterAccount user = new TwitterAccount(USER_NAME);
        user.post(SAMPLE_TWIT);

        List<Twit> wall = user.getWall();
        assertEquals(1, wall.size());
        assertEquals(SAMPLE_TWIT, wall.get(0).getText());

    }

    @Test
    public void shouldReturnOnlyUserTwits() {
        TwitterAccount account = new TwitterAccount(USER_NAME);
        account.post(SAMPLE_TWIT);
        Twit followedUserTwit = new Twit(SAMPLE_TWIT, FOLLOWED_USER);
        account.onTwit(followedUserTwit);

        List<Twit> wall = account.getWall();
        assertEquals(1, wall.size());
        assertEquals(account.getName(), wall.get(0).getAuthor());

    }

    @Test
    public void shouldSubscribeOnFollow(){
        TwitterAccount follower = new TwitterAccount(USER_NAME);
        TwitterAccount followed = mock(TwitterAccount.class);
        follower.follow(followed);

        verify(followed).addFollower(follower);

    }

    @Test
    public void shouldDisplayFollowedUserTwits() {
        TwitterAccount follower = new TwitterAccount(USER_NAME);
        TwitterAccount followed = new TwitterAccount(FOLLOWED_USER);
        follower.follow(followed);
        followed.post(SAMPLE_TWIT);

        List<Twit> timeLine = follower.getTimeLine();
        assertEquals(1, timeLine.size());
        assertEquals(SAMPLE_TWIT, timeLine.get(0).getText());

    }
}