package miku.twitter.api;

/*
Im not using java.util.Observable as it has synchronized blocks in addObserver and notifyObservers method
- in twitter i don't care about race condition between those methods.
 */
public interface Followable {


    void addFollower(Followable follower);

    void notifyFollowers(Twit twit);

    void onTwit(Twit twit);
}
