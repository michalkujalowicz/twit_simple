package miku.twitter.api;


import java.util.List;

public interface User {

     List<Twit> getWall();

     void post(String text);

     List<Twit> getTimeLine();

     void follow(Followable followable);

     String getName();
}
