package miku.twitter.api;

public class Twit {
    public static final int MAX_TWIT_SIZE = 140;

    private String author;

    private String text;

    public Twit(String text, String author) {
        if (text == null || text.length() > MAX_TWIT_SIZE) {
            throw new IllegalArgumentException("Twit text cannot be null and have to be shorter than "
                    + MAX_TWIT_SIZE + " characters");
        }
        this.text = text;
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Twit)) return false;

        Twit twit = (Twit) o;

        if (author != null ? !author.equals(twit.author) : twit.author != null) return false;
        return text != null ? text.equals(twit.text) : twit.text == null;
    }

    @Override
    public int hashCode() {
        int result = author != null ? author.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }
}
