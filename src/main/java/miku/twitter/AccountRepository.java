package miku.twitter;

import miku.twitter.model.TwitterAccount;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Repository
public class AccountRepository {

    private ConcurrentMap<String, TwitterAccount> accountsMap = new ConcurrentHashMap<>();

    public TwitterAccount findOrCreateAccount(String username) {
        TwitterAccount user = accountsMap.get(username);
        if (user == null) {

            user = createAccount(username);
        }
        return user;
    }

    private TwitterAccount createAccount(String username) {
        //putIfAbsent returns previous value (null if there is no object in map)
        // calling it twice is a simplest way to avoid synchronized block
        TwitterAccount newUser = accountsMap.putIfAbsent(username, new TwitterAccount(username));
        if (newUser == null) {
            newUser = accountsMap.putIfAbsent(username, new TwitterAccount(username));
        }
        return newUser;
    }
}
