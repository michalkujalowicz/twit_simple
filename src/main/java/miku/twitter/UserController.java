package miku.twitter;

import miku.twitter.api.Followable;
import miku.twitter.api.Twit;
import miku.twitter.api.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/twitter/user/{userName}", produces = {"application/json"})
public class UserController {
    @Autowired
    AccountRepository accountRepository;

    @RequestMapping("wall")
    public List<Twit> getWall(@PathVariable String userName) {
        return accountRepository.findOrCreateAccount(userName).getWall();
    }

    @RequestMapping(value = "twit", method = RequestMethod.POST, consumes = "text/plain")
     public ResponseEntity<?> post(@PathVariable String userName, @RequestBody String text) {
        if(text == null || text.length() > Twit.MAX_TWIT_SIZE){
            return ResponseEntity.badRequest().build();
        }

        accountRepository.findOrCreateAccount(userName).post(text);
        return ResponseEntity.ok().build();
    }

    @RequestMapping("timeline")
    public List<Twit> getTimeLine(@PathVariable String userName) {
        return accountRepository.findOrCreateAccount(userName).getTimeLine();
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = {"text/plain"})
    public void follow(@PathVariable String userName, @RequestBody String userToBeFollowed) {
        User follower = accountRepository.findOrCreateAccount(userName);
        Followable followable = accountRepository.findOrCreateAccount(userToBeFollowed);
        follower.follow(followable);
    }
}
