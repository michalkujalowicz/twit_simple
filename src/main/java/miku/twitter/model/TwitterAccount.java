package miku.twitter.model;

import miku.twitter.api.Followable;
import miku.twitter.api.Twit;
import miku.twitter.api.User;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;

public class TwitterAccount implements User, Followable {

    private String name = "";

    private List<Twit> twits = Collections.synchronizedList(new LinkedList<>());

    private Set<Followable> followers = new CopyOnWriteArraySet<>();

    public TwitterAccount(String name) {
        this.name = name;
    }

    public List<Twit> getWall() {
        return twits.stream().filter(twit -> twit.getAuthor().equals(name)).collect(Collectors.toList());
    }

    public void post(String text) {
        Twit twit = new Twit(text, name);
        onTwit(twit);
        notifyFollowers(twit);
    }

    public List<Twit> getTimeLine() {
        return twits.stream().filter(twit -> !twit.getAuthor().equals(name)).collect(Collectors.toList());
    }

    public void follow(Followable followable) {
        followable.addFollower(this);
    }

    @Override
    public void addFollower(Followable follower) {
        followers.add(follower);
    }

    @Override
    public void notifyFollowers(Twit twit) {
        followers.parallelStream().forEach(followable -> followable.onTwit(twit));
    }

    @Override
    public void onTwit(Twit twit) {
        twits.add(0,twit);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TwitterAccount)) return false;

        TwitterAccount that = (TwitterAccount) o;

        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
